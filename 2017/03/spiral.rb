#
# 37 36  35  34  33  32 31
# 38 17  16  15  14  13 30
# 39 18   5   4   3  12 29
# 40 19   6   1   2  11 28
# 41 20   7   8   9  10 27
# 42 21  22  23  24  25 26
# 43 44  45  46  47  48 49
#
square = $stdin.read.to_i

# Which square base are we on?
base = Math.sqrt(square).ceil
base += 1 if base.even?

# Where are we from the diagonal points
distance_from_diagonal = [
  (square % (base - 1) - 1).abs,
  (base - square % (base - 1))
].min

# Tada
res = (base - 1) - distance_from_diagonal
puts res

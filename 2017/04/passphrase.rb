require 'English'

valid = 0

while $stdin.gets
  duplicate_words = $LAST_READ_LINE.scan(/(\w+)/m).map(&:first).uniq!
  valid += 1 if duplicate_words.nil?
end

puts valid

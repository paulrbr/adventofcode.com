#
# Default Ruby stack size might limit you
#
# RUBY_THREAD_VM_STACK_SIZE=40000000 ruby instructions.rb < input
#
require 'English'

def jump(instructions, position, move, steps)
  steps += 1
  instructions[position] += 1
  next_position = position + move

  if next_position >= instructions.size
    puts steps
  else
    jump(instructions, next_position, instructions[next_position], steps)
  end
end

instructions = []

while $stdin.gets
  instructions << $LAST_READ_LINE.to_i
end

puts jump(instructions, 0, instructions.first, 0)

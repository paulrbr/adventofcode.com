input = $stdin.read.split('')[0..-2].map(&:to_i)

def find_next(list, first, acc = [])
  if list.empty?
    acc.sum
  else
    acc << list[0] if list[0] == (list[1] || first)

    find_next(list[1..-1], first, acc)
  end
end

puts find_next(input, input[0])

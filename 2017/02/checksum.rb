require "English"

def diff(list, min, max)
  if list.nil? || list.empty?
    max - min
  else
    if list.first < min
      max = min if min > max
      min = list.first
    elsif list.first > max
      min = max if max < min
      max = list.first
    end

    diff(list[1..-1], min, max)
  end
end

acc = 0

while $stdin.gets
  line = $LAST_READ_LINE.split(/ +/).map(&:to_i)
  if line[0] >= line[1]
    acc += diff(line, line[1], line[0])
  else
    acc += diff(line, line[1], line[0])
  end
end

puts acc
